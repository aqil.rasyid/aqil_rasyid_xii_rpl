<?php

class coursesModel {
    
    private $table = 'courses';
    private $db;

    public function __construct() {
        $this->db = new Database;
    }

    public function getAllCourses() {
        $this->db->query('SELECT * FROM '. $this->table);
        $this->db->execute();
        return $this->db->resultSet();
    }

    public function getSingleCourses($id) {
        $this->db->query('SELECT * FROM '. $this->table. ' WHERE id = :id');
        $this->db->bind('id',$id);

        return $this->db->single();
        // var_dump($id);
    }

    public function tambahDataCourses($data){
        $nama = $_FILES['img']['name'];
        $file_tmp = $_FILES['img']['tmp_name'];
        if(move_uploaded_file($file_tmp, 'img/' . $nama)){

            $query = "INSERT INTO courses VALUES ('',:category, :description, :pricing, :img)";

            $this->db->query($query);
            $this->db->bind('category', $data['category']);
            $this->db->bind('description', $data['description']);
            $this->db->bind('pricing', $data['pricing']);
            $this->db->bind('img', $nama);
            $this->db->execute();
        }
        return $this->db->rowCount();
    }

    public function ubahDataCourses($data) {

        if($_FILES['img']['name'] != "") {
            $nama = $_FILES['img']['name'];
            $file_tmp = $_FILES['img']['tmp_name'];
            move_uploaded_file($file_tmp, 'img/'.$nama);
        } else {
            $nama = $this->getSingleCourses($data['id'])['img'];
        }

        $query = "UPDATE courses 
            SET category = :category, 
                description = :description, 
                pricing = :pricing, img = :img 
                WHERE id = :id;";

            $this->db->query($query);
            $this->db->bind('category', $data['category']);
            $this->db->bind('description', $data['description']);
            $this->db->bind('pricing', $data['pricing']);
            $this->db->bind('img', $nama);
            $this->db->bind('id', $data['id']);

            $this->db->execute();

            return $this->db->rowCount();
    }

    public function deleteDataCourses($id){
        $query = "DELETE FROM courses WHERE id = :id";
        $this->db->query($query);
        $this->db->bind('id',$id);
        $this->db->execute();

        return $this->db->rowCount();
    }
}

?>