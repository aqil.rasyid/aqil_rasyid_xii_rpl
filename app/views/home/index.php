<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= BASEURL ?>assetmvc/css/bootstrap.min.css" >
    <link rel="stylesheet" href="<?= BASEURL ?>css/style.css">
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">

    <title>Hello, world!</title>
  </head>
  <body>

    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-light bg-white">
      <div class="container">
        <a class="navbar-brand" href="#">
          <img src="img/Logo.svg" alt="" loading="lazy">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item pt-1">
              <a class="nav-link" href="#"><b>Home</b></a>
            </li>
            <li class="nav-item pt-1">
              <a class="nav-link" href="#">Services</a>
            </li>
            <li class="nav-item pt-1">
              <a class="nav-link" href="#">Courses</a>
            </li>
            <li class="nav-item pt-1">
              <a class="nav-link" href="#">About Us</a>
            </li>
            <li class="nav-item pt-1">
              <a class="nav-link" href="#">Contact</a>
            </li>
            <button class="btn btn-primary oval">Login</button>
          </ul>
        </div>
      </div>
    </nav>
    <!-- Akhir navbar -->

    <!-- Jumbotron -->
    <div class="jumbotron jumbotron-fluid d-flex align-items-center mb-0">
      <div class="container">
        <div class="row">
          <div class="col-lg-8">
            <h1 class="display-4">Get Access to Unlimited Educational Resources. Everywhere, Everytime!</h1>
            <p class="lead">Premium access to more than 10,000 resources ranging from courses, events e.t.c</p>
            <button class="btn btn-primary">Get Access</button>
          </div>
        </div>
      </div>
    </div>
    <!-- Akhir jumbotron -->

    <!-- Services -->
    <section class="services d-flex align-items-center justify-content-center mt-0" id="services">
      <div class="container">
        <div class="row text-center">
          <div class="col-lg-12">
            <h4>Services</h4>
          </div>
        </div>
        <div class="row text-center">
          <div class="col-lg-4">
            <img src="img/service1.svg" alt="" srcset="">
            <h6>Unlimited Access</h6>
            <p>One subscription unlimited access</p>
          </div>
          <div class="col-lg-4">
            <img src="img/service2.svg" alt="" srcset="">
            <h6>Expert Teachers</h6>
            <p>Learn from industry expert who are passionate about teaching</p>
          </div>
          <div class="col-lg-4">
            <img src="img/service3.svg" alt="" srcset="">
            <h6>Learn Anywhere</h6>
            <p>Switch between your computer, tablet, or mobile device.</p>
          </div>
        </div>
      </div>
    </section>
    <!-- Akhir services -->

    <!-- Stories -->
    <section class="stories d-flex align-items-center" id="stories">
      <div class="container">
        <div class="row">
          <div class="col-lg-4">
            <h1>Sucess Stories From Our Students WorldWide!</h1>
            <p>Semaj Africa is an online education platform that delivers video courses, programs and resources for Individual, Advertising & Media Specialist, Online Marketing Professionals, Freelancers and anyone looking to pursue a career in digital marketing, Accounting, Web development, Programming. Multimedia and CAD design.</p>
            <button class="btn btn-primary">Discover</button>
          </div>
          <div class="col-lg-8 text-center">
            <img src="img/stories-img.png" alt="" width="80%">
          </div>
        </div>
      </div>
    </section>
    <!-- Akhir stories -->

    <!-- Courses -->
    <section class="courses d-flex align-items-center" id="courses">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h4>Courses</h4>
          </div>
        </div>
        <div class="row pb-4">
          <div class="col-lg-12 text-center">
            <a href="">All</a>
            <a href="">Design</a>
            <a href="">Web Development</a>
            <a href="">Digital</a>
            <a href="">Photography</a>
            <a href="">Motions Graphics</a>
            <a href="">Digital Marketing</a>
          </div>
        </div>
        <div class="row">
        <?php
        include "koneksi.php";
        $data = mysqli_query($koneksi, "SELECT * FROM courses");
        while ($d = mysqli_fetch_array($data)):
        ?>
          <div class="col-lg-3 col-md-3">
            <div class="card">
              <div class="card-top text-center pt-2">
                <?= $d['pricing']?>
              </div>
              <img src="img/<?= $d['img']?>" class="card-img-top" alt="...">
              <div class="card-body">
                <p><?= $d['description']?></p>
              </div>
            </div>
          </div>
        <?php
        endwhile;
        ?>  
      </div>
    </section>
    <!-- Akhir achievement -->

    <!-- Testimonial -->
    <section class="testimonials d-flex align-items-center" id="testimonials">
      <div class="container-fluid">
        <div class="row text-center">
          <div class="col-lg-12">
            <h1>What Students Say</h1>
          </div>
        </div>
        <div class="row text-center mb-3 justify-content-center">
          <div class="col-lg-8">
            <p>Semaj Africa is an online education platform that delivers video courses, programs and resources for Individual, Advertising & Media Specialist,.</p>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-3 mb-3">
            <div class="card">
              <div class="card-body">
                <img src="img/testi.svg" alt="">
                <p class="card-text pt-3 pb-3">Semaj Africa is an online education platform that delivers video courses, programs and resources for Individual, Advertising & Media Specialist, Online Marketing Professionals, Freelancers and anyone.</p>
                <div class="row">
                  <div class="col-lg-3">
                    <img src="img/User-Pic.svg" alt="">
                  </div>
                  <div class="col-lg-8 pt-2 ml-2">
                    <h5>Arthur Broklyn</h5>
                    <p>Categories: 3d Modelling</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 mb-3">
            <div class="card">
              <div class="card-body">
                <img src="img/testi.svg" alt="">
                <p class="card-text pt-3 pb-3">Semaj Africa is an online education platform that delivers video courses, programs and resources for Individual, Advertising & Media Specialist, Online Marketing Professionals, Freelancers and anyone.</p>
                <div class="row">
                  <div class=" mb-2">
                    <img src="img/User-Pic.svg" alt="">
                  </div>
                  <div class="col-lg-8 pt-2 ml-2">
                    <h5>Arthur Broklyn</h5>
                    <p>Categories: 3d Modelling</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 mb-3">
            <div class="card">
              <div class="card-body">
                <img src="img/testi.svg" alt="">
                <p class="card-text pt-3 pb-3">Semaj Africa is an online education platform that delivers video courses, programs and resources for Individual, Advertising & Media Specialist, Online Marketing Professionals, Freelancers and anyone.</p>
                <div class="row">
                  <div class="col-lg-3">
                    <img src="img/User-Pic.svg" alt="">
                  </div>
                  <div class="col-lg-8 pt-2 ml-2">
                    <h5>Arthur Broklyn</h5>
                    <p>Categories: 3d Modelling</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 mb-3"> 
            <div class="card">
              <div class="card-body">
                <img src="img/testi.svg" alt="">
                <p class="card-text pt-3 pb-3">Semaj Africa is an online education platform that delivers video courses, programs and resources for Individual, Advertising & Media Specialist, Online Marketing Professionals, Freelancers and anyone.</p>
                <div class="row">
                  <div class="col-lg-3">
                    <img src="img/User-Pic.svg" alt="">
                  </div>
                  <div class="col-lg-8 pt-2 ml-2">
                    <h5>Arthur Broklyn</h5>
                    <p>Categories: 3d Modelling</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Akhir testimonial -->

    <!-- Newsletter -->
    <section class="newsletter d-flex align-items-center" id="newsletter">
      <div class="container">
        <img src="img/Rectangle7.svg" alt="">
        <div class="pt-5 pl-5">
          <h3>Subscribe to Our Newsletter</h3>
          <p>Get exclusive discounts and latest news deliverd to your inbox for free!</p>
          <button class="btn btn-primary">Star Free Trial</button>
        </div>
      </div>
    </section>
    <!-- Akhir newsletter -->

    <!-- Footer -->
    <section class="footer d-flex align-items-center" id="footer">
      <div class="container">
        <div class="row mt-3">
          <div class="col-lg-3 col-md-12">
            <div class="mb-4">
              <img src="img/Logo.svg" alt="">
            </div>
            <span>
              Semaj Africa is an online education platform that delivers video courses, programs and resources.
            </span>
            <div class="mt-3 pb-4">
              <img src="img/sosmed.svg" alt="">
            </div>
          </div>
          <div class="col-lg-3 col-md-6">
            <div class="mb-4 pt-3">
              <b>Quicklinks</b>
            </div>
            <ul>
              <li><a href="#">Home</a></li>
              <li><a href="#">Courses</a></li>
              <li><a href="#">About Us</a></li>
              <li><a href="#">Contact Us</a></li>
            </ul>
          </div>
          <div class="col-lg-3 col-md-6">
            <div class="mb-4 pt-3">
              <b>Contact Us</b>
            </div>
            <div class="address">
              (+55) 254. 254. 254 Info@lsemajafrica.com Helios Tower 75 Tam Trinh Hoang Mai - Ha Noi - Viet Nam
            </div>
          </div>
          <div class="col-lg-3 col-md-12">
            <div class="pt-3">
              Terms and Conditions 
            </div>
            <div class="mb-4">Faq</div>
          </div>
        </div>
      </div>
    </section>
    <!-- Akhir footer -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="<?= BASEURL ?>/assetmvc/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
  </body>
</html>