<?php
// session_start();
// if (!isset($_SESSION['login'])) {
//     header('Location:../login/index.php');
//     exit;
// }
// echo "ini adalah Halaman index";
?>

<!-- <br>
<a href="../login/logout.php">Logout</a> -->

    <div class="container mt-5">
        <div class="row">

            <div class="col-lg-12">
                <?php Flasher::flash() ?>
            </div>

            <div class="col-lg-12">
                <h4>Daftar Courses</h4>
            </div>

            <div class="col-lg-6 mb-2">
                <!-- button modal -->
                <button type="button" class="btn btn-primary modalTambah" data-toggle="modal" data-target="#formModal">
                    Tambah Data
                </button>
            </div>
        </div>
        
        <div class="row">
            <table border="1" cellpadding="20">
                <thead>
                    <th>No</th>
                    <th>Category</th>
                    <th>Description</th>
                    <th>Pricing</th>
                    <th>Aksi</th>
                </thead>
                <tbody>
                <?php $i= 1?>
                <?php foreach($data['courses'] as $dt):?>
                    <tr>
                        <td><?= $i++ ?></td>
                        <td><?= $dt['category']?></td>
                        <td><?= $dt['description']?></td>
                        <td><?= $dt['pricing']?></td>
                        <td>
                            <a href="#" class="modalEdit" data-toggle="modal" data-target="#formModal" data-id="<?= $dt['id'] ?>">Edit</a> |
                            <a href="<?= BASEURL ?>courses/delete/<?= $dt['id']?>" onclick="return confirm('yakin?')">Hapus</a> |
                            <a href="<?= BASEURL ?>courses/detail/<?= $dt['id']?>">Detail</a>
                        </td>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>
        </div>
    </div>

    <div class="modal fade" id="formModal" tabindex="-1" aria-labelledby="formModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="formModalLabel">Tambah Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="form-modal" action="<?= BASEURL ?>courses/tambah" method="post" enctype="multipart/form-data">
                    <input type="hidden" id="id" name="id">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="category">Category</label>
                            <input type="text" class="form-control" id="category" name="category" placeholder="category">
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <input type="text" class="form-control" id="description" name="description" placeholder="description">
                        </div>
                        <div class="form-group">
                            <label for="pricing">Pricing</label>
                            <select name="pricing" id="pricing" class="form-control">
                                <option value="Free">Free</option>
                                <option value="Premium">Premium</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="img">Image</label>
                            <input type="file" class="form-control" id="img" name="img">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Tambah Data</button>
                    </div>
                </form>
            </div>
        </div>
    </div>