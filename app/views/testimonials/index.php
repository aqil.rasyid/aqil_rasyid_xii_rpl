
<div class="container mt-5">
        <div class="row">
            <div class="col-lg-12">
                Daftar Testimonials
            </div>
        </div>
        <div class="row">
            <table border="1" cellpadding="20">
                <thead>
                    <th>No</th>
                    <th>Category</th>
                    <th>Description</th>
                    <th>Pricing</th>
                </thead>
                <tbody>
                <?php $i= 1?>
                <?php foreach($data['testimonials'] as $dt):?>
                    <tr>
                        <td><?= $i++ ?></td>
                        <td><?= $dt['category']?></td>
                        <td><?= $dt['name']?></td>
                        <td><?= $dt['testimoni']?></td>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>
        </div>
    </div>