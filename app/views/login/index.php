<?php
session_start();

include "../koneksi.php";

$error = false;

if (isset($_COOKIE['id']) && isset($_COOKIE['username'])) {
    $id = $_COOKIE['id'];
    $username = $_['username'];

    $result = mysqli_query($koneksi,"SELECT * FROM users WHERE id = '$id' ");
    $row = mysqli_fetch_assoc($result);

    if ($username = $row['username']) {
        $_SESSION['login'] = true;
    }
}

if (isset($_SESSION['login'])) {
    header('Location: ../courses/index.php');
    exit;
}

if (isset($_POST['login'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];


    $result = mysqli_query($koneksi,"SELECT * FROM users WHERE username = '$username' ");
    
    if (mysqli_num_rows($result) == 1) {
        $row = mysqli_fetch_assoc($result);

        if (md5($password) == $row['password']) {
            $_SESSION['login'] = true;

            if (isset($_POST['remember'])) {
                setcookie('id',$row['id'],time()+60);
                setcookie('username',$row['username'],time()+60);
            }
            header('Location: ../courses/index.php');
            exit;
        }
    }else{
        $error = true;
    }

}

?>

<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Login</title>

        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

        <link href="../asset/css/login.css" rel="stylesheet">
        </head>
    <body>
        <form class="form-signin" action="" method="POST">
            <h1 class="h3 mb-3 font-weight-normal text-center">Login</h1>
            <p style="color: red; font-style:italic" <?php echo $error == true ? '' : 'hidden'; ?> >Username atau Password salah</p>
            <label for="inputusername" class="sr-only">Username</label>
            <input type="text" id="inputusername" name="username" class="form-control" placeholder="Username" required autofocus>
            <label for="inputPassword" class="sr-only">Password</label>
            <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required>
            <div class="checkbox mb-3">
                <label>
                <input type="checkbox" value="remember"> Remember me
                </label>
            </div>
            <button class="btn btn-lg btn-primary btn-block" type="submit" name="login">Login</button>
        </form>
    </body>
</html>