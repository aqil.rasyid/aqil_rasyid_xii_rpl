<?php

class Courses extends Controller {

    public function index() {

        $data['courses'] = $this->model('coursesModel')->getAllCourses();

        $this->view('templates/header', $data);
        $this->view('courses/index',$data);
        $this->view('templates/footer',$data);
    }

    public function tambah()
    {
        if($this->model('coursesModel')->tambahDataCourses($_POST) > 0){
            Flasher::setFlash('berhasil','ditambahkan','success');
            header('Location:'. BASEURL . '/courses');
            exit;
        } else {
            Flasher::setFlash('gagal','ditambahkan','danger');
            header('Location:' . BASEURL . '/courses');
            exit;
        }
    }

    public function detail($id) {
        $data['courses'] = $this->model('coursesModel')->getSingleCourses($id);
        $this->view('templates/header', $data);
        $this->view('courses/detail',$data);
        $this->view('templates/footer',$data);
    }

    public function getUbah(){
        echo json_encode($this->model('coursesModel')->getSingleCourses($_POST['id']));
    }

    public function ubah() {
        if($this->model('coursesModel')->ubahDataCourses($_POST) > 0){
            Flasher::setFlash('berhasil','diubah','success');
            header('Location:' . BASEURL . '/courses');
            exit;
        }
        else {
            Flasher::setFlash('gagal','diubah','danger');
            header('Location:' . BASEURL . '/courses');
            exit;
        }
    }

    public function delete($id){
        if($this->model('coursesModel')->deleteDataCourses($id) > 0){
            Flasher::setFlash('berhasil','dihapus','success');
            header('Location:' . BASEURL . '/courses');
            exit;
        }else {
            Flasher::setFlash('gagal','dihapus','danger');
            header('Location:' . BASEURL > '/courses');
            exit;
        }
    }
}

?>