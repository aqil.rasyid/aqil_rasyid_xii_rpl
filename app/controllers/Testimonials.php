<?php

class Testimonials extends Controller{

    public function index(){
        $data['testimonials'] = [
            [
                'category' => 'Digital Marketing',
                'name' => 'Arthur Brokyln 01',
                'testimoni' => 'Semaj Africa is an online education platform that delivers video courses, programs and resources for Individual, Advertising & Media Specialist, Online Marketing Professionals, Freelancers and anyone.'
            ],
            [
                'category' => 'Design',
                'name' => 'Arthur Brokyln 02',
                'testimoni' => 'Semaj Africa is an online education platform that delivers video courses, programs and resources for Individual, Advertising & Media Specialist, Online Marketing Professionals, Freelancers and anyone.'
            ],
            [
                'category' => 'Digital Marketing',
                'name' => 'Arthur Brokyln 03',
                'testimoni' => 'Semaj Africa is an online education platform that delivers video courses, programs and resources for Individual, Advertising & Media Specialist, Online Marketing Professionals, Freelancers and anyone.'
            ],
            [
                'category' => 'Design',
                'name' => 'Arthur Brokyln 04',
                'testimoni' => 'Semaj Africa is an online education platform that delivers video courses, programs and resources for Individual, Advertising & Media Specialist, Online Marketing Professionals, Freelancers and anyone.'
            ],
            [
                'category' => 'Digital Marketing',
                'name' => 'Arthur Brokyln 05',
                'testimoni' => 'Semaj Africa is an online education platform that delivers video courses, programs and resources for Individual, Advertising & Media Specialist, Online Marketing Professionals, Freelancers and anyone.'
            ],
        ];

        $this->view('templates/header',$data);
        $this->view('testimonials/index',$data);
        $this->view('templates/footer',$data);

    }
}

?>