-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 07 Sep 2020 pada 01.54
-- Versi server: 10.4.13-MariaDB
-- Versi PHP: 7.2.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_pweb`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `courses`
--

CREATE TABLE `courses` (
  `id` int(11) NOT NULL,
  `category` varchar(100) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `pricing` varchar(100) DEFAULT NULL,
  `img` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `courses`
--

INSERT INTO `courses` (`id`, `category`, `description`, `pricing`, `img`) VALUES
(1, 'Digital Marketing', 'Become A Social Media Expert', 'Free', 'undraw_Note_list_re_r4u9.svg'),
(2, 'Design', 'Advance Your 3d Modelling Skill', 'Free', 'undraw_personal_email_t7nw.svg'),
(3, 'Digital Marketing', 'The Art of Growing Relationship', 'Premium', 'undraw_processing_thoughts_d8ha.svg'),
(4, 'Design', 'Skills Needed In Becoming A Designer', 'Free', 'undraw_respond_8wjt.svg'),
(5, 'Digital Marketing', 'Become A Social Media Expert', 'Free', 'undraw_Note_list_re_r4u9.svg'),
(6, 'Design', 'Advance Your 3d Modelling Skill', 'Free', 'undraw_personal_email_t7nw.svg'),
(7, 'Digital Marketing', 'The Art of Growing Relationship', 'Premium', 'undraw_processing_thoughts_d8ha.svg'),
(8, 'Design', 'Skills Needed In Becoming A Designer', 'Free', 'undraw_respond_8wjt.svg'),
(9, 'Digital Marketing', 'Become A Social Media Expert', 'Free', 'undraw_Note_list_re_r4u9.svg'),
(10, 'Design', 'Advance Your 3d Modelling Skill', 'Free', 'undraw_personal_email_t7nw.svg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `username`, `password`) VALUES
(1, 'admin', '202cb962ac59075b964b07152d234b70');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
