$(function(){

    $('.modalTambah').on('click', function(){

        $('#formModalLabel').html('Tambah Data');
        $('.modal-footer button[type=submit]').html('Tambah Data');
        $('input[type="text"]').val('');
        $('#pricing').prop('selectedIndex',0);
    });

    $('.modalEdit').on('click', function(){
        console.log(1);
        $('#formModalLabel').html('Edit Data');
        $('.modal-footer button[type=submit]').html('Ubah Data');
        $('.form-modal').attr('action','http://localhost:8080/aqil_rasyid_xii_rpl/public/courses/ubah')

        const id = $(this).data('id');
        $.ajax({
            url: 'http://localhost:8080/aqil_rasyid_xii_rpl/public/courses/getUbah',
            data : { id : id },
            method : 'post',
            dataType : 'json',
            success : function(data) {
                $('#category').val(data.category);
                $('#description').val(data.description);
                $('#pricing').val(data.pricing);
                $('#id').val(data.id);
            }
        })
    });
});